package BusinessLogic;

/**
 * Enum representing all the possible commands
 */
public enum Operation {
    InsertClient,
    DeleteClient,
    InsertProduct,
    DeleteProduct,
    Order,
    Report
}
